package com.cognizant.tictactoe.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class Controller {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public String simpleGet() {
        return "Congrats, this actually works";
    }

}
